import math


def get_pretty_input():
    print("Enter the cost of a weekly subscription to the app:")
    sub_cost = float(input())
    print("Enter a percent of app installers who are expected to choose this subscription option:")
    percent_sub = float(input())
    print("Enter an average 'life expectancy' of a paid user:")
    life_exp = float(input())
    print("Enter the coefficient of free (organic) installations:")
    organic_inst = float(input())
    print("Enter CPI:")
    cpi = float(input())
    print("Enter a refund rate:")
    refund_rate = float(input())
    print("Enter desired net profit:")
    net_profit = float(input())
    print("Enter a percentage of App Store commission and paid traffic costs:")
    commission = float(input())
    return sub_cost, percent_sub, life_exp, organic_inst, cpi, refund_rate, net_profit, commission


def get_input():
    print("Enter all arguments:")
    return map(float, input().split())


if __name__ == '__main__':
    # change type of input if you want
    # input = 7 3 10 10 1.1 5 10000 30
    sub_cost, percent_sub, life_exp, organic_inst, cpi, refund_rate, net_profit, commission = get_input()

    profit_before_commission = net_profit / (1 - commission / 100)
    installs = math.ceil(profit_before_commission / (sub_cost * life_exp * percent_sub / 100 * (1 - refund_rate / 100) -
                                           (1 - organic_inst / 100) * cpi))
    ads_expenditure = installs * (1 - organic_inst / 100) * cpi
    profit_from_subs = sub_cost * life_exp * percent_sub / 100 * (1 - refund_rate / 100) * installs

    print(f"Number of installs: {math.ceil(installs)} \n"
          f"Expenditure: {round(ads_expenditure, 2)}\n")

"""
    Answers for input parameters
    Number of installs: 14215
    Expenditure: 14072.85
"""