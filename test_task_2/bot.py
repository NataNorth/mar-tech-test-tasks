import requests, json
from bottle import run, post, response, request as bottle_request
from apscheduler.schedulers.background import BackgroundScheduler

BOT_URL = 'https://api.telegram.org/bot1497998511:AAEoHYGCYk9cDL3_azuc62Bu2POv8wETzmo/'

api_token = '463560988db040bfa286b7cf25f52ff5'
api_url_base = 'https://openexchangerates.org/api/'

scheduler = BackgroundScheduler()


def get_latest_rate(to='UAH'):

    api_url = api_url_base + 'latest.json'

    response = requests.get(api_url, params={'app_id': api_token})

    if response.status_code == 200:
        return response.json()['rates'][to]
    else:
        print("Something went wrong with rates link")


def save_rate(rate):
    with open('storage.json', 'r') as openfile:
        json_object = json.load(openfile)

    json_object['previous_rate'] = rate
    with open('storage.json', 'w') as outfile:
        json.dump(json_object, outfile)


def get_prev_rate_from_storage():
    with open('storage.json') as json_file:
        return json.load(json_file)['previous_rate']


def save_chat_id(chat_id):
    with open('storage.json', 'r') as openfile:
        json_object = json.load(openfile)
        json_object['chat_ids'].append(chat_id)
        json_object['chat_ids'] = list(set(json_object['chat_ids']))
    with open('storage.json', 'w') as outfile:
        json.dump(json_object, outfile)


def get_chat_ids_from_storage():
    with open('storage.json', 'r') as openfile:
        return json.load(openfile)['chat_ids']


def get_chat_id(data):
    """
    Extract chat id from telegram request if it is a first message or bot is a new member of a chat.
    """
    if 'my_chat_member' in data and data['my_chat_member']['new_chat_member']['status'] == 'member':
        chat_id = data['my_chat_member']['chat']['id']
    elif 'message' in data and 'text' in data['message'] and data['message']['text'] == '/start':
        chat_id = data['message']['chat']['id']
    else:
        chat_id = None

    return chat_id


def send_message(prepared_data):
    message_url = BOT_URL + 'sendMessage'
    requests.post(message_url, json=prepared_data)


def get_data_for_answer(chat_id, rate, diff):
    json_data = {
        "chat_id": chat_id,
        "text": f"Current exchange rate from USD to UAH: \n{rate} (↑ {diff} grn)",
    }
    return json_data


def get_introductory_message(chat_id, rate):
    json_data = {
        "chat_id": chat_id,
        "text": f"Hi! My purpose is to tell you exchange rate if it grows.\n"
                f"Current exchange rate from USD to UAH: \n{rate}",
    }
    return json_data


@post('/')
def main():
    chat_data = bottle_request.json
    rate = get_prev_rate_from_storage()
    if not rate:
        rate = get_latest_rate()
        save_rate(rate)

    chat_id = get_chat_id(chat_data)
    if chat_id:
        answer_data = get_introductory_message(chat_id, rate)
        send_message(answer_data)

        save_chat_id(chat_id)  # in order to send scheduled messages

    return response


def scheduled_notification():
    latest_rate = get_latest_rate()
    previous_rate = get_prev_rate_from_storage()

    if latest_rate > previous_rate:
        chat_ids = get_chat_ids_from_storage()
        for chat_id in chat_ids:
            send_message(get_data_for_answer(chat_id, latest_rate, round(latest_rate - previous_rate, 2)))
    save_rate(latest_rate)


if __name__ == '__main__':
    job = scheduler.add_job(scheduled_notification, 'interval', minutes=60)
    scheduler.start()
    run(host='localhost', port=8080, debug=True)